import sun.java2d.opengl.WGLSurfaceData;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.security.PublicKey;
import java.util.Date;

public class Builder  {


    class SliderListener implements ChangeListener
    {
        public void stateChanged(ChangeEvent event)
        {
            JSlider slider = (JSlider)event.getSource();
            int value = 1000/slider.getValue();

            for(int i=0; i<WorkerArr.length; i++)
                WorkerArr[i].timeout = value;

        }

    }


public class Worker extends Thread{
    Date dat;
    Stock MyStock;
    Stock KuzovStok;
    Stock EngineStok;
    Stock AccessoryStock;
    int timeout;

    Worker (Stock C, Stock E, Stock K, Stock A) {
        CarNumber = 0;
        MyStock = C;
        EngineStok = E;
        KuzovStok = K;
        AccessoryStock = A;

    }


    public void run() {
        try{
            while (true)
            {
                MyTasks.get();

                Detal k= KuzovStok.get();
                Detal e = EngineStok.get();
                Detal a = AccessoryStock.get();
                CarNumber++;

                dat = new Date();
                String id =dat+"____"+CarNumber+":"+ k.ID +":"+e.ID+":"+a.ID;
                Detal d = new Detal(id, MakerType) ;

                sleep(timeout);

                MyStock.put(d);


            }
        }
        catch (InterruptedException e) {
            System.out.println("error in Zavod" + Detal.DtypeToString(MakerType));
        }
    }



}




    public Tasks MyTasks;

    int CarNumber;
    Worker[] WorkerArr;
    SliderListener SL = new SliderListener();

    Detal.DetalType MakerType = Detal.DetalType.C;

    Builder (Stock C, Stock E, Stock K, Stock A, int WorkCount)
    {
        WorkerArr = new Worker[WorkCount];

        for(int i=0; i<WorkerArr.length; i++)
            WorkerArr[i] = new Worker(C, E, K, A);


        MyTasks = new Tasks();

    }

public void start()
{
    for(int i=0; i<WorkerArr.length; i++)
        WorkerArr[i].start();
}

public void SetTimeout(int val){
    for(int i=0; i<WorkerArr.length; i++)
        WorkerArr[i].timeout = val;
}



}
