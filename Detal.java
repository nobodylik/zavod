public class Detal {
    String ID;
    DetalType type;

    public enum DetalType{
        E,  // engine
        K, // kuzov
        C, // car
        A; // Aksesuar

    }


    public static String DtypeToString(Detal.DetalType d)
    {
        if(d == Detal.DetalType.E)
            return "Eng";
        else if(d == Detal.DetalType.K)
            return "Kuz";
        else if(d == Detal.DetalType.C)
            return "Сar";
        else
            return "Aks";

    }

    Detal(String id, DetalType t)
    {
        type =t;
        ID = id;
  //      System.out.println("New Detal: " +Stock.DtypeToString(t)+ " ID:" + ID);
    }
}

