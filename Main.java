import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.FileHandler;


public class Main {
    public static final Logger logger = Logger.getGlobal();

    public static void main(String[] args) {
        try {
            if(true)
            {
                FileHandler fh;
                //fh = new FileHandler("C:\\Users\\user\\source\\repos\\java-calc\\MyLogFile.log");
                fh = new FileHandler("MyLogFile.log");
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter();
                fh.setFormatter(formatter);
                logger.setUseParentHandlers(false); // отключает вывод в консоль
                logger.info("Logger start \n");
            }

        }
        catch (IOException e) {
            System.err.println("Error while reading file: " + e.getLocalizedMessage());
        }

        ZavodView theView = new ZavodView();

        ZavodModel theModel = new ZavodModel();

        ZavodController theController = new ZavodController(theView, theModel);

        theView.setVisible(true);

    }

}
