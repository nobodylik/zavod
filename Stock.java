import java.util.*;

public class Stock {
    int MaxSize = 1000;
    int SendCount=0;
    int AllReciveCount=0;
    Detal.DetalType DetType;

    ArrayDeque<Detal> MyQueue = new  ArrayDeque<Detal>();

    Stock(Detal.DetalType d)
    {
        DetType = d;
    }

    public synchronized void put(Detal d)
    {

        // если склад полон, то поток, который пытается положить новую деталь ждет пока не освободиться место

        while (MyQueue.size()>=MaxSize) {  // проверяем есть ли свободное место на складе
            try {
                wait();  // поток снимет блокировку и переходит в режим ожидания, ждет пока кто-то не вызвал Notify()
            }
            catch (InterruptedException e) {
            }
        }

        MyQueue.add(d);
        AllReciveCount++;
        notify(); // снимаем блокирвку и опвещаем всех кто ждал доступ к этому складу


        System.out.println("Stock " +Detal.DtypeToString(DetType)+ " have: "+ MyQueue.size());


    }

    public synchronized Detal get()
    {
        // если склад пуст, то поток, который пытается взять деталь ждет пока не появится новая деталь

        while (MyQueue.size()<1) {
            try {
                wait(); // поток снимет блокировку и переходит в режим ожидания, ждет пока кто-то не вызвал Notify()

            }
            catch (InterruptedException e) {
            }
        }

       Detal d =MyQueue.poll();
       SendCount++;
       notify(); // снимаем блокирвку и опвещаем всех кто ждал доступ к этому складу
       return d;
    }



    public int GetCount()
    {
        return MyQueue.size();
    }
}
