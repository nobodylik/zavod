import com.sun.org.apache.xml.internal.security.Init;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class ZavodController {

    private ZavodView theView;
    private ZavodModel theModel;
    public TimerTask timerTask;
    public Timer timer;
DSliderListener DeallerListener;
ASliderListener AccessListener;
    class DSliderListener implements ChangeListener
    {
        public void stateChanged(ChangeEvent event)
        {

            JSlider slider = (JSlider)event.getSource();
            int value = 1000/slider.getValue();
            for (int i=0; i< theModel.Deal.length; i++)
            theModel.Deal[i].timeout = value;
        }

    }

    class ASliderListener implements ChangeListener
    {
        public void stateChanged(ChangeEvent event)
        {

            JSlider slider = (JSlider)event.getSource();
            int value = 1000/slider.getValue();
            for (int i=0; i< theModel.AccessMaker.length; i++)
                theModel.AccessMaker[i].timeout = value;
        }

    }



    public ZavodController(ZavodView theView, ZavodModel theModel) {
        this.theView = theView;
        this.theModel = theModel;
        this.theView.setVisible(true);
        Init();
        DeallerListener = new DSliderListener();
        AccessListener = new ASliderListener();
        this.theView.addSEngineListener(theModel.EngineMaker.SL);
        this.theView.addSKuzovListener(theModel.KuzovMaker.SL);
        this.theView.addSCarListener(theModel.CarMaker.SL);
        this.theView.addSDeallerListener(DeallerListener);
        this.theView.addSAccessListener(AccessListener);


        timerTask = new MyTimerTask();

        timer = new Timer(true);
        // будем запускать каждых 10 секунд (10 * 1000 миллисекунд)
        timer.scheduleAtFixedRate(timerTask, 0, 1*200);


        theModel.AllStart();
    }


    public void Init()
    {
        int Etimeout=1; // engine maker timeout
        theView.SEngine.setValue(Etimeout);
        theModel.EngineMaker.timeout=1000/Etimeout;

        int Ktimeout=1; // kuzov maker timeout
        theView.SKuzov.setValue(Ktimeout);
        theModel.KuzovMaker.timeout=1000/Ktimeout;

        int Ctimeout=1; // Car maker timeout
        theView.SCar.setValue(Ctimeout);
        theModel.CarMaker.SetTimeout(1000/Ctimeout);

        int Dtimeout=1; // Dealler maker timeout
        for (int i=0; i<theModel.Deal.length; i++)
        {
            theView.SDealler.setValue(Dtimeout);
            theModel.Deal[i].timeout=1000/Dtimeout;
        }

        int Atimeout=1; // Accessory maker timeout
        for (int i=0; i<theModel.AccessMaker.length; i++)
        {
            theView.SAccess.setValue(Atimeout);
            theModel.AccessMaker[i].timeout=1000/Atimeout;
        }

        theView.TFAccessCount.setText(Integer.toString (theModel.ASu));
        theView.TFCarBuilderCount.setText(Integer.toString (theModel.WC));
        theView.TFDeallerCount.setText(Integer.toString (theModel.D));

    }



    public class MyTimerTask extends TimerTask {
        @Override
        public void run()
        {
            theView.TFAllCarsEverCount.setText(Integer.toString (theModel.CarStock.AllReciveCount));
            theView.TFEngineStock.setText(Integer.toString (theModel.EngineStock.GetCount()));
            theView.TFKuzovStock.setText(Integer.toString (theModel.KuzovStock.GetCount()));
            theView.TFCarStock.setText(Integer.toString (theModel.CarStock.GetCount()));
            theView.TFDeallerAllSold.setText(Integer.toString (theModel.CarStock.SendCount));
            theView.TFAccessStock.setText(Integer.toString (theModel.AccessorStock.GetCount()));
        }
    }

}