import java.io.IOException;
import java.util.logging.Level;

public class ZavodModel {

    INIFile F;


    Controller CarContr;

    int WC;
    int ASu;
    int D;


    Stock EngineStock;
    Maker EngineMaker ;

    Stock KuzovStock;
    Maker KuzovMaker;

    Stock AccessorStock;
    Maker[] AccessMaker;


    Stock CarStock;
    Builder CarMaker;


    Dealler[] Deal;
    ZavodModel()
    {
        try
        {
            F = new INIFile("inicialfile.ini");
        }
        catch (IOException e)
        {
                System.out.println("cant open inifile");
        }
        EngineStock = new Stock(Detal.DetalType.E);
        int ES = F.getProperty("Parameters", "StorageMotorSize",1000);
        EngineStock.MaxSize=ES;
        EngineMaker = new Maker(EngineStock);

        KuzovStock = new Stock(Detal.DetalType.K);
        int KS = F.getProperty("Parameters", "StorageBodySize",1000);
        KuzovStock.MaxSize=KS;
        KuzovMaker = new Maker(KuzovStock);

        AccessorStock = new Stock(Detal.DetalType.A);
        int AS = F.getProperty("Parameters", "StorageAccessorySize",1000);
        AccessorStock.MaxSize=AS;

        CarStock = new Stock(Detal.DetalType.C);
        int CS = F.getProperty("Parameters", "StorageAutoSize",1000);
        CarStock.MaxSize=CS;

         WC = F.getProperty("Parameters", "Workers",10);
        CarMaker = new Builder(CarStock, EngineStock, KuzovStock, AccessorStock,WC );


        CarContr = new Controller(CarMaker.MyTasks, CarStock);

        boolean LogOn = F.getProperty("Parameters", "LogSale", true);
        if(!LogOn)
            Main.logger.setLevel(Level.OFF);
        //EngineMaker.timeout =1;
      //  KuzovMaker.timeout =3;
      //  CarMaker.timeout = 1;

      //  CarMaker = new Builder[5];

         ASu = F.getProperty("Parameters", "AccessorySuppliers",10);
        AccessMaker = new Maker[ASu];

         D = F.getProperty("Parameters", "Dealers",10);
        Deal = new Dealler[D]; ////5 диллеров

      /*  for(int i=0; i<CarMaker.length; i++)
        {
            CarMaker[i] = new Builder(CarStock, EngineStock, KuzovStock, AccessorStock);
        }*/

        for(int i=0; i<Deal.length; i++)
        {
            Deal[i] = new Dealler(CarStock);
        }

        for(int i=0; i<AccessMaker.length; i++)
        {
            AccessMaker[i] = new Maker(AccessorStock);
        }


    }
    public void AllStart()
    {
        EngineMaker.start();
        KuzovMaker.start();
        CarMaker.start();
        CarContr.start();



        for (int i=0; i<Deal.length; i++)
        {
            Deal[i].start();
        }

        for (int i=0; i<AccessMaker.length; i++)
        {
            AccessMaker[i].start();
        }

    }


}
