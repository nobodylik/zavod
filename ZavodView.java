import java.awt.*;
import javax.swing.*;
import javax.swing.event.ChangeListener;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class ZavodView extends JFrame{

    public JSlider SEngine = new JSlider(1, 31, 10);
    public JSlider SKuzov = new JSlider(1, 31, 10);
    public JSlider SAccess = new JSlider(1, 31, 10);
    public JSlider SDealler = new JSlider(1, 31, 10);
    public JSlider SCar = new JSlider(1, 31, 10);

    //    private JSlider SEngine = new JSlider(100, 20100, 10000);

    private JLabel LAccessZavodName  = new JLabel("Accessory Stock", JLabel.CENTER);
    public JTextField TFAccessStock  = new JTextField(10);
    public JTextField TFAccessCount  = new JTextField(10);

    private JLabel LEngineZavodName  = new JLabel("Engine Stock", JLabel.CENTER);
    public JTextField TFEngineStock  = new JTextField(10);

    private JLabel LKuzovZavodName  = new JLabel("Kuzov Stock", JLabel.CENTER);
    public JTextField TFKuzovStock  = new JTextField(10);

    private JLabel LCarZavodName  = new JLabel("Car Stock", JLabel.CENTER);
    public JTextField TFCarStock  = new JTextField(10);
    public JTextField TFCarBuilderCount  = new JTextField(10);
    private JLabel LCarAll  = new JLabel("produced cars", JLabel.CENTER);
    public JTextField TFAllCarsEverCount  = new JTextField(10);

    private JLabel LDeallerName  = new JLabel("Dealler buy", JLabel.CENTER);
    public JTextField TFDeallerAllSold  = new JTextField(10);
    public JTextField TFDeallerCount  = new JTextField(10);

    ZavodView()
    {
        JPanel ZavodPanel = new JPanel();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1100, 700);
        this.setResizable(false);

        ZavodPanel.setLayout(null);



        LAccessZavodName.setBounds(750, 70, 80, 20);
        TFAccessStock.setBounds(750, 90, 80, 20);
        TFAccessStock.setText("0");
        TFAccessStock.setFocusable(false);
        TFAccessCount.setBounds(830, 70, 40, 20);
        TFAccessCount.setText("0");
        TFAccessCount.setFocusable(false);



        LEngineZavodName.setBounds(150, 70, 80, 20);
        TFEngineStock.setBounds(150, 90, 80, 20);
        TFEngineStock.setText("0");
        TFEngineStock.setFocusable(false);


        LKuzovZavodName.setBounds(450, 70, 80, 20);
        TFKuzovStock.setBounds(450, 90, 80, 20);
        TFKuzovStock.setText("0");
        TFKuzovStock.setFocusable(false);


        LCarZavodName.setBounds(450, 240, 80, 20);
        TFCarStock.setBounds(450, 260, 80, 20);
        TFCarStock.setText("0");
        TFCarStock.setFocusable(false);
        TFCarBuilderCount.setBounds(530, 240, 40, 20);
        TFCarBuilderCount.setText("0");
        TFCarBuilderCount.setFocusable(false);
        TFAllCarsEverCount.setBounds(730, 280, 80, 20);
        LCarAll.setBounds(719, 260, 100, 20);
        TFCarBuilderCount.setText("0");
        TFCarBuilderCount.setFocusable(false);



        LDeallerName.setBounds(450, 400, 80, 20);
        TFDeallerAllSold.setBounds(450, 420, 80, 20);
        TFDeallerAllSold.setText("0");
        TFDeallerAllSold.setFocusable(false);
        TFDeallerCount.setBounds(530, 400, 40, 20);
        TFDeallerCount.setText("0");
        TFDeallerCount.setFocusable(false);

        SEngine.setBounds(90, 120, 200, 50);

        SEngine.setMajorTickSpacing(10);
        SEngine.setMinorTickSpacing(5);
        SEngine.setPaintLabels(true);
        SEngine.setPaintTicks(true);


        SKuzov.setBounds(390, 120, 200, 50);

        SKuzov.setMajorTickSpacing(10);
        SKuzov.setMinorTickSpacing(5);
        SKuzov.setPaintLabels(true);
        SKuzov.setPaintTicks(true);


        SDealler.setBounds(390, 450, 200, 50);

        SDealler.setMajorTickSpacing(10);
        SDealler.setMinorTickSpacing(5);
        SDealler.setPaintLabels(true);
        SDealler.setPaintTicks(true);


        SCar.setBounds(390, 290, 200, 50);

        SCar.setMajorTickSpacing(10);
        SCar.setMinorTickSpacing(5);
        SCar.setPaintLabels(true);
        SCar.setPaintTicks(true);



        SAccess.setBounds(690, 120, 200, 50);

        SAccess.setMajorTickSpacing(10);
        SAccess.setMinorTickSpacing(5);
        SAccess.setPaintLabels(true);
        SAccess.setPaintTicks(true);




        ZavodPanel.add(LEngineZavodName);
        ZavodPanel.add(TFEngineStock);
        ZavodPanel.add(LKuzovZavodName);
        ZavodPanel.add(TFKuzovStock);
        ZavodPanel.add(LCarZavodName);
        ZavodPanel.add(TFCarStock);
        ZavodPanel.add(TFCarBuilderCount);
        ZavodPanel.add(TFAllCarsEverCount);
        ZavodPanel.add(LCarAll);
        ZavodPanel.add(LDeallerName);
        ZavodPanel.add(TFDeallerAllSold);
        ZavodPanel.add(TFDeallerCount);
        ZavodPanel.add(LAccessZavodName);
        ZavodPanel.add(TFAccessStock);
        ZavodPanel.add(TFAccessCount);

        ZavodPanel.add(SEngine);
        ZavodPanel.add(SDealler);
        ZavodPanel.add(SKuzov);
        ZavodPanel.add(SCar);
        ZavodPanel.add(SAccess);
        add(ZavodPanel);
    }


    void addSEngineListener(ChangeListener listenForSlider){
        SEngine.addChangeListener(listenForSlider);
    }

    void addSDeallerListener(ChangeListener listenForSlider){
        SDealler.addChangeListener(listenForSlider);
    }

    void addSKuzovListener(ChangeListener listenForSlider){
        SKuzov.addChangeListener(listenForSlider);
    }

    void addSCarListener(ChangeListener listenForSlider){
        SCar.addChangeListener(listenForSlider);
    }

    void addSAccessListener(ChangeListener listenForSlider){
        SAccess.addChangeListener(listenForSlider);
    }
}

